package Common_Methods;

import Environment_and_Repository.RequestRepo;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class API_Trigger extends RequestRepo {

	static String headername = "Content-Type";
	static String headervalue = "application/json";

	public static Response Post_API_Trigger(String requestBody , String endpoint) {

		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername, headervalue);
		req_spec.body(requestBody);
		Response response = req_spec.post(endpoint);
		return response;
	}
	
	public static Response Get_API_Trigger(String endpoint) {

		RequestSpecification req_spec = RestAssured.given();
		Response response = req_spec.get(endpoint);
		return response;
	}

	public static Response Put_API_Trigger(String requestBody , String endpoint) {

		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername, headervalue);
		req_spec.body(requestBody);
		Response response = req_spec.put(endpoint);
		return response;
	}
	
	public static Response Patch_API_Trigger(String requestBody , String endpoint) {

		RequestSpecification req_spec = RestAssured.given();
		req_spec.header(headername, headervalue);
		req_spec.body(requestBody);
		Response response = req_spec.patch(endpoint);
		return response;
	} 
	
	public static Response Delete_API_Trigger(String requestBody , String endpoint) {

		RequestSpecification req_spec = RestAssured.given();
		Response response = req_spec.delete(endpoint);
		return response;
	}
}
