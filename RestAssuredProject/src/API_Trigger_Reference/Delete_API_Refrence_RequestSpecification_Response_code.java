package API_Trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Delete_API_Refrence_RequestSpecification_Response_code {

	public static void main(String[] args) {
		
		//Step1: Declare the needed variables
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		
		// Step 2: Trigger the API
		// Step 2.1: Build the request specification using RequestSpecification
		RequestSpecification req_spec = RestAssured.given();
		
		// Step 2.4: Trigger the API
		Response response = req_spec.delete(hostname + resource);
		
		// Step 3: Extract the status code
		int statusCode = response.statusCode();
		System.out.println(statusCode);

	}

}
