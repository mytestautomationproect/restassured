package API_Trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;
import io.restassured.path.json.JsonPath;

public class Post_API_Trigger_Refrence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Step1:Declare the needed variables
		
		String hostname = "https://reqres.in";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";
		
		// Step2: Configure the API & Trigger
		RestAssured.baseURI = hostname;
		
		// Step3 : Configure the API for execution and log entire transaction (request header , request body , response header , response body , time etc)
		//given().header(headername,headervalue).body(requestBody).log().all().when().post(resource).then().log().all().extract().response().asString();
		
		//Step4: Configure the API for execution and save the response in a String variable
		String responseBody = given().header(headername,headervalue).body(requestBody).when().post(resource).then().extract().response().asString();
		System.out.println(responseBody);
		
		//Step5: Parse the response body
		//Step5.1: create the object
		JsonPath jsp_res = new JsonPath(responseBody);
		
		// Step5.2: Parse individual parameters using jsp_res object 
		String res_name = jsp_res.getString("name");
		System.out.println(res_name);
		
		String res_job = jsp_res.getString("job");
		System.out.println(res_job);
		
		String res_id = jsp_res.getString("id");
		System.out.println(res_id);
		
		String res_createdAt = jsp_res.getString("createdAt");
		res_createdAt = res_createdAt.substring(0, 10);
		System.out.println(res_createdAt);
		
		// Step6: Validate the response body
		// Step6.1: Parse request body and save into local variables
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		// Step 6.2 : Generate expected date
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0,10);
		
		// Step 6.3 : Use TestNG's Assert
		Assert.assertEquals(res_name, req_name,"Name in responseBody is equal to name sent from requestBody");
		Assert.assertEquals(res_job, req_job,"job in responseBody is equal to job sent from requestBody");
		Assert.assertNotNull(res_id,"id in responseBody is not null");
		Assert.assertEquals(res_createdAt, expecteddate,"createdAt in responseBody is equal to expecteddate");
		
		
		/* Assertion failure
		 * Assert.assertNotEquals(res_name, req_name,"ijk");
		 */
		
	}

}
