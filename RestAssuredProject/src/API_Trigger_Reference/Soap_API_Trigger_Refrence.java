package API_Trigger_Reference;

import io.restassured.RestAssured;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.path.xml.XmlPath;

public class Soap_API_Trigger_Refrence {

	public static void main(String[] args) {
		
		// Step 1 : Collect all needed information and save it into local variables

		RestAssured.useRelaxedHTTPSValidation();
		
		String req_body = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n"
				+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\r\n"
				+ "  <soap:Body>\r\n"
				+ "    <NumberToWords xmlns=\"http://www.dataaccess.com/webservicesserver/\">\r\n"
				+ "      <ubiNum>28</ubiNum>\r\n"
				+ "    </NumberToWords>\r\n"
				+ "  </soap:Body>\r\n"
				+ "</soap:Envelope>";
		
		String hostname = "https://www.dataaccess.com";
		String resource = "/webservicesserver/NumberConversion.wso";
		String headername = "Content-Type";
		String headervalue = "application/xml";
		
		// Step 2 : Declare BaseURI
		RestAssured.baseURI = hostname;
		
		// Step 3 : Configure the API for execution and save the response in a String variable
		String res_body =given().header(headername,headervalue).body(req_body).when().post(resource).then().extract().asString();
		
		System.out.println(res_body);
		
		// Step 4 : Parse the response body

		// Step 4.1 : Create the object of XmlPath
		XmlPath xml_res = new XmlPath(res_body);
		
		// Step 4.2 : Parse individual params using xml_res object
		String result = xml_res.getString("NumberToWordsResult");
		
		// Step 5 : Validate the response body
		Assert.assertEquals(result,"twenty eight ");
		
		
	}

}
