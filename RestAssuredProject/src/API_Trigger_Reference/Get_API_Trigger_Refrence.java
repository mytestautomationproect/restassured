package API_Trigger_Reference;

import static org.testng.Assert.ARRAY_MISMATCH_TEMPLATE;
import java.util.List;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import static io.restassured.RestAssured.given;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;

public class Get_API_Trigger_Refrence {

	public static void main(String[] args) {

		// Step 1: Declare the expected results

		// Step 1.1: Declare expected results of page parameters
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		// Step 1.2 : Declare expected results of data array
		int[] id = { 7, 8, 9, 10, 11, 12 };
		String[] email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] first_name = {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String[] last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};


		// Step 3: Declare the needed variables
		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";


		// Step4: Configure the API & Trigger
		RestAssured.baseURI = hostname;

		String responseBody = given().when().get(resource).then().extract().response().asString();
		System.out.println(responseBody);

		//given().when().get(resource).then().log().all().extract().response().asString();

		//Step5: Parse the response body
		//Step5.1: create the object
		JsonPath jsp_res = new JsonPath(responseBody);
				
		// Step 5.2: Fetch page parameters
		int res_page = jsp_res.getInt("page");
		int res_per_page = jsp_res.getInt("per_page");
		int res_total = jsp_res.getInt("total");
		int res_total_pages = jsp_res.getInt("total_pages");
		
		// Step 6: Fetch Size of data array
		List<String> dataArray = jsp_res.getList("data");
		int sizeofarray = dataArray.size();
		
		// Step 7: Validation

		// Step 7.1: Validate page parameters
		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);

		// Step 7.2 : Validate data array
		for (int i = 0; i < sizeofarray; i++) 
		{
			Assert.assertEquals(Integer.parseInt(jsp_res.getString("data[" + i + "].id")), id[i], "Validation of id failed : " + i);
			Assert.assertEquals(jsp_res.getString("data[" + i + "].email"), email[i],"Validation of email failed : " + i);
			Assert.assertEquals(jsp_res.getString("data[" + i + "].first_name"), first_name[i],"Validation of first_name failed : " + i);
			Assert.assertEquals(jsp_res.getString("data[" + i + "].last_name"), last_name[i],"Validation of last_name failed : " + i);
			Assert.assertEquals(jsp_res.getString("data[" + i + "].avatar"), avatar[i],"Validation of avatar failed : " + i);
		}

		// Step 8: Declare expected results of support
		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		// Step 8.1: Validate support JSON
		Assert.assertEquals(jsp_res.getString("support.url"), exp_url, "Validation of URL failed");
		Assert.assertEquals(jsp_res.getString("support.text"), exp_text,"Validation of text failed" );

	}

}
