package API_Trigger_Reference;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class Delete_API_Trigger_Refrence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		
		
		// Step2: Configure the API & Trigger
		RestAssured.baseURI = hostname;

		String responseBody = given().when().delete(resource).then().extract().response().asString();
		System.out.println(responseBody);
		
		//given().when().delete(resource).then().log().all().extract().response().asString();
		
		
}

	}
