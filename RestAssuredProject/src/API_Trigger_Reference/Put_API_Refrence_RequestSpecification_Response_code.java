package API_Trigger_Reference;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Put_API_Refrence_RequestSpecification_Response_code {

	public static void main(String[] args) {
		
		//Step1: Declare the needed variables
		
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";
		
		
		// Step 2: Trigger the API
		// Step 2.1: Build the request specification using RequestSpecification
		RequestSpecification req_spec = RestAssured.given();
		
		// Step 2.2: Set the header
		req_spec.header(headername,headervalue);
		
		// Step 2.3: Set the request body
		req_spec.body(requestBody);
		
		// Step 2.4: Trigger the API
		Response response = req_spec.put(hostname + resource);
		
		// Step 3: Extract the status code
		int statusCode = response.statusCode();
		System.out.println(statusCode);
		
		// Step 4: Fetch the response body parameters
		ResponseBody responseBody = response.getBody();
		System.out.println(responseBody.asString());
		
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0,10);
		
		// Step 5: Fetch the request body parameters
		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		// Step 6: Generate expected data
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0,10);
		
		// Step 7 : Validate using TestNG Assertions
		Assert.assertEquals(res_name, req_name,"Name in ResponseBody is equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, exp_date, "createdAt in ResponseBody is equal to Date Generated");
		
	}

}
