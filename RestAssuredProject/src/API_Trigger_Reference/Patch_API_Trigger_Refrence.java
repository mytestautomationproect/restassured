package API_Trigger_Reference;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Patch_API_Trigger_Refrence {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Step1: Declare the needed variables
		
				String hostname = "https://reqres.in";
				String resource = "/api/users/2";
				String headername = "Content-Type";
				String headervalue = "application/json";
				String requestBody = "{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}";
				
				// Step2: Configure the API & Trigger
				RestAssured.baseURI = hostname;
				
				String responseBody = given().header(headername,headervalue).body(requestBody).when().patch(resource).then().extract().response().asString();
				System.out.println(responseBody);
				
				//given().header(headername,headervalue).body(requestBody).log().all().when().patch(resource).then().log().all().extract().response().asString();
				
				//Step5: Parse the response body
				//Step5.1: create the object
				JsonPath jsp_res = new JsonPath(responseBody);
				
				// Step5.2: Parse individual parameters using jsp_res object 
				String res_name = jsp_res.getString("name");
				System.out.println(res_name);
				
				String res_job = jsp_res.getString("job");
				System.out.println(res_job);
				
				
				String res_updatedAt = jsp_res.getString("updatedAt");
				res_updatedAt = res_updatedAt.substring(0, 10);
				System.out.println(res_updatedAt);
				
				// Step6: Validate the response body
				// Step6.1: Parse request body and save into local variables
				JsonPath jsp_req = new JsonPath(requestBody);
				String req_name = jsp_req.getString("name");
				String req_job = jsp_req.getString("job");
				
				// Step 6.2 : Generate expected date
				LocalDateTime currentdate = LocalDateTime.now();
				String expecteddate = currentdate.toString().substring(0,10);
				
				// Step 6.3 : Use TestNG's Assert
				Assert.assertEquals(res_name, req_name,"Name in responseBody is equal to name sent from requestBody");
				Assert.assertEquals(res_job, req_job,"Job in responseBody is equal to job sent from requestBody");
				Assert.assertEquals(res_updatedAt, expecteddate,"updatedAt in responseBody is equal to updatedAt sent from requestBody");
	}

}
