package oop_concept;

public class single_inheritance extends encapsulation_family {
	
	int numofkids;
	int numofadults;

	public void familyContans() {
		
		System.out.println("Family have " +numofkids+ " kids and " +numofadults+ " adults in the persons family");
		
	}
	
	public static void main(String[] args) {
		
		single_inheritance inherit_obj = new single_inheritance();
		
		inherit_obj.numofkids = 4;
		inherit_obj.numofadults = 6;
		inherit_obj.numofFamMember = 10;
		inherit_obj.numofRelatives = 20;
		
		inherit_obj.familyContans();
		inherit_obj.family();
		inherit_obj.relatives();
		
	}
	 
}
