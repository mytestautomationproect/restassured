package oop_concept;

public class method_overload_family {

	public void family_contains(int homes) {
		System.out.println("A persons family have "+homes+ " homes");
	}
	
	public void family_contains(String homes) {
		System.out.println("A persons family have "+homes+ " homes");
	}
	
	public void family_contains(int homes, String farmhouse) {
		System.out.println("A persons family have "+homes+ " homes and " +farmhouse+ " farmhouses");
	}
	public static void main(String[] args) {
		method_overload_family prop_obj = new method_overload_family();
		
		prop_obj.family_contains(2);
		prop_obj.family_contains("two");
		prop_obj.family_contains(2,"two");
		
	}

}
