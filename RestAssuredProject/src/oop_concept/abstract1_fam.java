package oop_concept;

public class abstract1_fam extends abstract_family {
	
	@Override
	public void fam_kids() {
		System.out.println("A persons family have 4 kids");
	}

	@Override
	public void fam_adults() {
		System.out.println("A persons family have 6 adults");
		
	}

	public static void main(String[] args) {
		abstract1_fam fam = new abstract1_fam();
		
		fam.fam_kids();
		fam.fam_adults();
		
		fam.fam_members();
		

	}

	

}
