package oop_concept;

public abstract class abstract_family {
	
	//abstract method [unimplemented]
	public abstract void fam_kids();
	
	public abstract void fam_adults();
	
	//non abstract method [implemented]
	public void fam_members() {
		System.out.println("Persons family have 10 members");
	}

}
