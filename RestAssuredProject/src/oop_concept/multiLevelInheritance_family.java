package oop_concept;

public class multiLevelInheritance_family extends single_inheritance {
	String familyhaveparents;
	String familyhavekids;
	
	public void familyhave() {
		System.out.println("A persons family have "+familyhaveparents+ " and "+familyhavekids);
	}
	
	
	public static void main(String[] args) {
		multiLevelInheritance_family obj1 = new multiLevelInheritance_family();
		
		obj1.familyhaveparents = "Mother & father";
		obj1.familyhavekids = "two son's and two daughters";
		obj1.familyhave();
		
		obj1.numofkids = 4;
		obj1.numofadults = 6;
		obj1.numofFamMember = 10;
		obj1.numofRelatives = 20;
		obj1.familyContans();
		obj1.family();
		obj1.relatives();
		

	}

}
