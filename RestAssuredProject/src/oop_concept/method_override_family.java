package oop_concept;

public class method_override_family {
	
	public void family_contains(int homes) {
		System.out.println("A persons family have "+homes+ " homes");
	}
    
	public void fam_details(int fam_members, int kids) {
		System.out.println("A persons family have "+fam_members+ " and have " +kids+ " kids in the family");
	}
	
	public void fam_details(String fam_members, String kids) {
		System.out.println("A persons family have "+fam_members+ " and have " +kids+ " kids in the family");
	}
	public static void main(String[] args) {
		method_override_family fam_obj = new method_override_family();
		
		fam_obj.family_contains(2);
		fam_obj.fam_details(10,5);
		fam_obj.fam_details("ten","five");
		
		
		

	}

}
