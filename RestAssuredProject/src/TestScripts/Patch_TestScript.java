package TestScripts;

import java.time.LocalDateTime;
import org.testng.Assert;

import Common_Methods.API_Trigger;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_TestScript extends API_Trigger {

	public static void execute() {
	
		Response response = Patch_API_Trigger(patch_request_body(),patch_endpoint());
		
		int statusCode = response.statusCode();
		System.out.println(statusCode);
		
		ResponseBody responseBody = response.getBody();
		System.out.println(responseBody.asString());
		
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0,10);
		
		JsonPath jsp_req = new JsonPath(patch_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0,10);
		
		Assert.assertEquals(res_name, req_name,"Name in responseBody is equal to name sent from requestBody");
		Assert.assertEquals(res_job, req_job,"Job in responseBody is equal to job sent from requestBody");
		Assert.assertEquals(res_updatedAt, exp_date,"updatedAt in responseBody is equal to updatedAt sent from requestBody");
		

	}

}
