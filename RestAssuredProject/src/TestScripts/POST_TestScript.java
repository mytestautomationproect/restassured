package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import Common_Methods.API_Trigger;
import Common_Methods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class POST_TestScript extends API_Trigger {

	public static void execute() throws IOException {

		File logfolder = Utilities.create_folder("Post_API");
		int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = Post_API_Trigger(post_requestBody(),post_endpoint());
			int statusCode = response.statusCode();
			System.out.println(statusCode);
			if (statusCode == 201) {
				ResponseBody responseBody = response.getBody();
				System.out.println(responseBody.asString());
				
				Utilities.create_log_file("Post_API_TC1", logfolder, post_endpoint(), post_requestBody(),
						response.getHeaders().toString(), responseBody.asString());
				validate(responseBody);
				break;
			} else {
				System.out.println("Status code found in iteration :" + i + " is :" + statusCode
						+ ", and is not equal expected status code hence retrying");
			}
		}

		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times");
	}	
		
		private static void validate(ResponseBody responseBody) {
		
			String res_name = responseBody.jsonPath().getString("name");
			String res_job = responseBody.jsonPath().getString("job");
			String res_id = responseBody.jsonPath().getString("id");
			String res_createdAt = responseBody.jsonPath().getString("createdAt");
			res_createdAt = res_createdAt.toString().substring(0,10);
			
			
			JsonPath jsp_req = new JsonPath(post_requestBody());
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");
			
			
			LocalDateTime curr_date = LocalDateTime.now();
			String exp_date = curr_date.toString().substring(0,10);
			
			Assert.assertEquals(res_name, req_name,"Name in ResponseBody is equal to Name sent in Request Body");
			Assert.assertEquals(res_job, req_job, "Job in ResponseBody is equal to Job sent in Request Body");
			Assert.assertNotNull(res_id, "Id in ResponseBody is not null");
			Assert.assertEquals(res_createdAt, exp_date, "createdAt in ResponseBody is equal to Date Generated");		
	
	}


}
