package TestScripts;

import java.time.LocalDateTime;
import org.testng.Assert;
import Common_Methods.API_Trigger;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_TestScript extends API_Trigger {

	public static void execute() {
	
		Response response = Put_API_Trigger(put_request_body() ,put_endpoint());
		
		int statusCode = response.statusCode();
		System.out.println(statusCode);
		

		ResponseBody responseBody = response.getBody();
		System.out.println(responseBody.asString());
		
		
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0,10);
		
		JsonPath jsp_req = new JsonPath(put_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0,10);
		
		Assert.assertEquals(res_name, req_name,"Name in ResponseBody is equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, exp_date, "updatedAt in ResponseBody is equal to Date Generated");
		
	}
	
}

