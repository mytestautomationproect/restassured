package TestScripts;

import java.util.List;

import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_TestScript extends API_Trigger {

	@Test
	public void execute()  {
	
	Response response = Delete_API_Trigger(delete_request_body() , delete_endpoint());
	
	// Step 5: Extract the status code
	int statuscode = response.statusCode();
	System.out.println(statuscode);
	
	}

}
